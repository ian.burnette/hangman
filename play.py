import words
from game_logic import guess, reveal

if __name__ == "__main__":
    WORD = words.get()[0]
    state = (reveal(WORD, ""))
    while "".join(state[1]) !=  WORD and len(state[0])<5:
        print("\n"*100)
        print(" ".join(state[1]) + "\n")
        print("incorrect guesses: " + "".join(state[0]))
        print(str(5-len(state[0])) + " chances remaining.")
        this_guess = input("Guess a letter (or digit):")
        state = guess(WORD, this_guess, state)
    if len(state[0])<5:
        print("\n" * 100)
        print(" ".join(state[1]) + "\n")
        print("You Won!")
    else:
        print("\n" * 100)
        print(" ".join(state[1]) + "\n")
        print("You Lost :(")
