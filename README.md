# Hangman

A simple "hangman"-style game.

Clone the repository and cd into the directory. Next, install dependencies using 
a venv is always encouraged to avoid conflicts, but at the very least use the --user option

`pip install -r requirements.txt`

Next setup the database:
`sqlite3 sqlite3.db < schema.sql`

start the server with:
`./start`

By default flask will serve on `http://127.0.0.1:5000/`. The game can be played
at the index of the site, and scores can be viewed at `http://127.0.0.1:5000/scores`.

Alternatively, a very simple cli version of the game can be played using `python play.py`
