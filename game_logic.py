import re

# error messages
NON_ALPHANUMERIC = "Non-alphanumeric received."
INVALID_GAME_STATE = "Received impossible game state."


def reveal(word, guesses):
    """
    Accepts a word and a string of guesses and produces a list of incorrect
    guesses, and partially revealed word. Unguessed characters are replaced with
    "_".

    Both the incorrect guesses, and the partially revealed word and returned as
    lists of characters.

    :param word: The word the player is trying to guess
    :param guesses: The guesses the player has answered
    :return: (missing_chars:list, revealed:list) where:
        missing_chars is a list of incorrect guesses
        revealed is the word, but with unguessed characters replaced with "_"
    """
    is_invalid = re.match(".*[^0-9A-Za-z]+.*", guesses)
    if is_invalid:
        raise ValueError("Non-alphanumeric received.")
    word = word.upper()
    guesses = guesses.upper()
    missing_chars = []
    revealed = ["_"] * len(word)
    for g_char in guesses:
        if g_char in missing_chars or g_char in revealed:
            continue
        for i, w_char in enumerate(word):
            if g_char == w_char:
                revealed[i] = g_char
        if g_char not in revealed:
            missing_chars.append(g_char)
    return missing_chars, revealed


def guess(word, my_guess, state):
    """
    Accepts a word, a single character guess, and a game state

    we strip "_" from the game state and append the latest guess to the end

    We call reveal on the resulting "guesses" in order to produce a new gamestate.
    """
    if len(word) != len(state[1]) or re.match(".*[^0-z]+.*", "".join(state[1])):
        raise ValueError(INVALID_GAME_STATE)
    current_missed = "".join(state[0])
    current_revealed = "".join(state[1]).replace("_", "")
    guesses = current_missed + my_guess + current_revealed
    return reveal(word, guesses)
