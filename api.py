from . import words
from .game_logic import guess, reveal
from . import scores
import json
from flask import Flask, render_template, request
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def game():
    """
    This address handles the actual playing of the game.
    """
    if request.method == "GET":
        # If GET then start a new game
        word, salt, hash = words.get()
        missed, revealed = reveal(word, "")
    else:
        # Otherwise continue an existing one
        word, salt, hash = words.get(request.form["salt"],
                                     request.form["hash"])
        state = json.loads(request.form["state"])
        missed, revealed = guess(word, request.form["guess"], state)

    data = {
        "revealed": revealed,
        "salt": salt,
        "hash": hash,
        "remaining": 5 - len(missed),
        "missed": missed,
        "state": (missed, revealed),
    }
    if "_" not in revealed:
        # You've won!
        data["score"] = len(revealed)*10 - len(missed)*5
        return render_template("won.html", **data)
    elif len(missed) == 5:
        return render_template("lose.html", **data)

    return render_template("main.html", **data)


@app.route('/scores', methods=['GET', 'POST'])
def scoreboard():
    score = 0
    name = ""
    if request.method == "POST":
        score = request.form["score"]
        name = request.form["name"]
        scores.record(name, score)
    data = {
        "scores": scores.retrieve(),
        "score": score,
        "name": name,
    }
    return render_template("scores.html", **data)
