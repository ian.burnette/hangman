import sqlite3
import os
from flask import g, Flask
app = Flask(__name__)

DATABASE = os.path.join(os.getcwd(), "sqlite3.db")


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    db.row_factory = sqlite3.Row
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def record(name, score):
    name = str(name)[:10]
    insert = "INSERT INTO topscores VALUES (?, ?)"
    get_db().execute(insert, (name, score))
    get_db().commit()


def retrieve():
    ordered = "SELECT * FROM topscores ORDER BY score DESC, name ASC"
    return query_db(ordered)[:10]
