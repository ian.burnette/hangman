import pytest
from game_logic import guess as _guess
from game_logic import INVALID_GAME_STATE, NON_ALPHANUMERIC

WORD = "ORDER"

TESTS = [
    # Tests Incorrect Guess
    (([], list("_____")), "a", (list("A"), list("_____"))),
    ((list("B"), list("_____")), "a", (list("BA"), list("_____"))),
    ((list("A"), list("_____")), "a", (list("A"), list("_____"))),
    (([], list("_R__R")), "a", (list("A"), list("_R__R"))),
    ((list("B"), list("_R__R")), "a", (list("BA"), list("_R__R"))),
    ((list("A"), list("_R__R")), "a", (list("A"), list("_R__R"))),
    (([], list(WORD)), "a", (list("A"), list(WORD))),
    ((list("BQTYUI"), list("_R__R")), "a", (list("BQTYUIA"), list("_R__R"))),
    ((list("BQTYUI"), list(WORD)), "a", (list("BQTYUIA"), list(WORD))),
    
    # Tests Correct Guess
    (([], list("_____")), "o", ([], list("O____"))),
    ((list("B"), list("_____")), "o", (list("B"), list("O____"))),
    ((list("A"), list("_____")), "o", (list("A"), list("O____"))),
    (([], list("_R__R")), "o", ([], list("OR__R"))),
    ((list("B"), list("_R__R")), "o", (list("B"), list("OR__R"))),
    ((list("A"), list("_R__R")), "o", (list("A"), list("OR__R"))),
    (([], list(WORD)), "o", ([], list(WORD))),
    ((list("BQTYUI"), list("_R__R")), "o", (list("BQTYUI"), list("OR__R"))),
    ((list("BQTYUI"), list(WORD)), "o", (list("BQTYUI"), list(WORD))),
]


# Tests that uppercase guesses are handled correctly
def capitalize_test(t):
    new_t = list(t)
    new_t[1] = new_t[1].upper()
    return tuple(new_t)


CAPITALIZED_TESTS = [capitalize_test(t) for t in TESTS]
TESTS += CAPITALIZED_TESTS
TESTS += [
    # Tests invalid game states
    (([], list("")), "o", ValueError(INVALID_GAME_STATE)),
    (([], list(" ")), "o", ValueError(INVALID_GAME_STATE)),
    (([], list("O!DE!")), "o", ValueError(INVALID_GAME_STATE)),
    (([], list("ORDERS")), "o", ValueError(INVALID_GAME_STATE)),
    (([], list("******")), "o", ValueError(INVALID_GAME_STATE)),
    
    # Tests invalid guesses
    (([], list("_____")), "!", ValueError(NON_ALPHANUMERIC)),
    (([], list("_____")), " ", ValueError(NON_ALPHANUMERIC)),
    (([], list("_____")), "_", ValueError(NON_ALPHANUMERIC)),
]



@pytest.mark.parametrize("state, guess, expected", TESTS)
def test_guess(state, guess, expected):
    if isinstance(expected, Exception):
        with pytest.raises(type(expected)) as e:
            _guess(WORD, guess, state)
        assert str(e.value) == expected.args[0]
    else:
        assert _guess(WORD, guess, state) == expected
