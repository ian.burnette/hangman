import pytest
from game_logic import reveal, NON_ALPHANUMERIC

SIMPLE_WORD = "ORDER"
NUMERIC_WORD = "3DHUBS"


def no_match(s):
    return ["_"]*len(s)


TESTS = [
    # Tests Missing Characters
    (SIMPLE_WORD, "i", (list("I"), no_match(SIMPLE_WORD))),
    (SIMPLE_WORD, "ai", (list("AI"), no_match(SIMPLE_WORD))),
    (SIMPLE_WORD, "aiu", (list("AIU"), no_match(SIMPLE_WORD))),
    (NUMERIC_WORD, "i", (list("I"), no_match(NUMERIC_WORD))),
    (NUMERIC_WORD, "ai", (list("AI"), no_match(NUMERIC_WORD))),
    (NUMERIC_WORD, "ai2", (list("AI2"), no_match(NUMERIC_WORD))),
    
    # Tests Missing Characters >= 5 (lost game)
    (SIMPLE_WORD, "aiust", (list("AIUST"), no_match(SIMPLE_WORD))),
    (SIMPLE_WORD, "aiustb", (list("AIUSTB"), no_match(SIMPLE_WORD))),
    (NUMERIC_WORD, "ai2jxc", (list("AI2JXC"), no_match(NUMERIC_WORD))),
    
    # Tests Matching Characters
    (SIMPLE_WORD, "o", ([], list("O____"))),
    (SIMPLE_WORD, "r", ([], list("_R__R"))),
    (SIMPLE_WORD, "or", ([], list("OR__R"))),
    (SIMPLE_WORD, "ro", ([], list("OR__R"))),
    (NUMERIC_WORD, "d", ([], list("_D____"))),
    (NUMERIC_WORD, "3", ([], list("3_____"))),
    (NUMERIC_WORD, "d3", ([], list("3D____"))),
    (NUMERIC_WORD, "3d", ([], list("3D____"))),

    # Tests Partial Matches
    (SIMPLE_WORD, "orai", (list("AI"), list("OR__R"))),
    (SIMPLE_WORD, "raio", (list("AI"), list("OR__R"))),
    (NUMERIC_WORD, "aid", (list("AI"), list("_D____"))),
    (NUMERIC_WORD, "a3i2", (list("AI2"), list("3_____"))),
    (SIMPLE_WORD, "aiurst", (list("AIUST"), list("_R__R"))),
    (SIMPLE_WORD, "ariuostb", (list("AIUSTB"), list("OR__R"))),
    (NUMERIC_WORD, "ai2jxbs3c", (list("AI2JXC"), list("3___BS"))),

    # Tests Repeat Chars
    (SIMPLE_WORD, "oo", ([], list("O____"))),
    (SIMPLE_WORD, "oro", ([], list("OR__R"))),
    (SIMPLE_WORD, "ii", (list("I"), no_match(SIMPLE_WORD))),
    (SIMPLE_WORD, "iai", (list("IA"), no_match(SIMPLE_WORD))),
    (SIMPLE_WORD, "orraai", (list("AI"), list("OR__R"))),
    (SIMPLE_WORD, "oraiio", (list("AI"), list("OR__R"))),

    # Tests Wins
    (SIMPLE_WORD, "odraie", (list("AI"), list("ORDER"))),
    (SIMPLE_WORD, "draioe", (list("AI"), list("ORDER"))),
    (NUMERIC_WORD, "aidsb3hu", (list("AI"), list("3DHUBS"))),
    (NUMERIC_WORD, "3ia2sbhud", (list("IA2"), list("3DHUBS"))),

    # Tests a "win" that is actually a loss
    (SIMPLE_WORD, "draixzvoe", (list("AIXZV"), list("ORDER"))),
]


# Tests that uppercase guesses are handled correctly
def capitalize_test(t):
    new_t = list(t)
    new_t[1] = new_t[1].upper()
    return tuple(new_t)


CAPITALIZED_TESTS = [capitalize_test(t) for t in TESTS]
TESTS += CAPITALIZED_TESTS

TESTS += [
    # Tests Empty Guesses
    (SIMPLE_WORD, "", ([], no_match(SIMPLE_WORD))),
    (NUMERIC_WORD, "", ([], no_match(NUMERIC_WORD))),

    # Tests Invalid Characters
    (SIMPLE_WORD, " ", ValueError(NON_ALPHANUMERIC)),
    (SIMPLE_WORD, "!", ValueError(NON_ALPHANUMERIC)),
    (SIMPLE_WORD, "_", ValueError(NON_ALPHANUMERIC)),
    (SIMPLE_WORD, "o!as", ValueError(NON_ALPHANUMERIC)),
    (SIMPLE_WORD, "orde!", ValueError(NON_ALPHANUMERIC)),
    (SIMPLE_WORD, "qweasdwqrfs!", ValueError(NON_ALPHANUMERIC)),
]


@pytest.mark.parametrize("word, guesses, expected", TESTS)
def test_reveal(word, guesses, expected):
    if isinstance(expected, Exception):
        with pytest.raises(type(expected)) as e:
            reveal(word, guesses)
        assert str(e.value) == expected.args[0]
    else:
        assert reveal(word, guesses) == expected
