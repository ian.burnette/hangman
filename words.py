import hashlib
import random

WORDS = ['3DHUBS', 'MARVIN', 'PRINT', 'FILAMENT', 'ORDER', 'LAYER']


def get(salt=None, hash=None):
    if bool(salt) ^ bool(hash):
        raise ValueError("Both salt and hash should be defined")
    elif not salt:
        word = random.choice(WORDS)
        salt = hashlib.sha256().hexdigest()
        hash = hashlib.sha256((salt+word).encode()).hexdigest()
    else:
        hash_table = {hashlib.sha256((salt+w).encode()).hexdigest() : w for w in WORDS}
        word = hash_table.get(hash, None)
        if not word:
            raise ValueError("Word not found.")
    return word, salt, hash
